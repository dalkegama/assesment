import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DashboardComponent } from './dashboard.component';
import { DataTableModule } from '../dataTable/dataTable.module';
import { SearchModule } from '../search/search.module';
import { SpinnerComponent } from '../shared/spinner/spinner.component';


@NgModule({
  imports: [
    CommonModule,
    SearchModule,
    DataTableModule
  ],
  declarations: [
    SpinnerComponent,
    DashboardComponent,
  ]
})
export class DashboardModule { }
