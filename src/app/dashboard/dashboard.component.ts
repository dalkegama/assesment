import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { AccountDataItem, AccountDataItems, SanitizedAccountResult } from './account/account.data';
import { AccountDataService } from './account/account.data.service';
import { AccountResultsService } from './account/account.results.service';
import { SearchSettings } from '../search/search.settings';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  searchOptions: SearchSettings;
  accountDataSubscription: Subscription;
  accountDataItems: AccountDataItem[];
  tableColumns: string[];
  sanitizedAccountResults: SanitizedAccountResult[];
  showSpinner: boolean;

  constructor(
    private account: AccountDataService,
    private getResult: AccountResultsService
  ) {
    this.searchOptions = {
      placeHolder: 'Enter an account Name or ID',
      searchFieldLabel: 'Search for an account by account name or ID',
      minLength: 2,
      maxLength: 100
    };
    this.tableColumns = [ 'accountID', 'accountName', 'accountNumber', 'sortCode', 'balance'];
    this.showSpinner = true;
  }

  ngOnInit(): void  {
    this.getDashboardData();
  }

  ngOnDestroy(): void  {
    this.accountDataSubscription.unsubscribe();
  }

  updateSearch(searchTerm): void  {
    const term = searchTerm.trim();

    if (term !== '') {
      this.sanitizedAccountResults = this.getResult.newAccountDetailsList(term, this.sanitizedAccountResults);
    } else {
      this.showSpinner = true;
      this.getDashboardData();
    }
  }

  private getDashboardData(): void  {
    this.accountDataSubscription = this.account.loadResults()
      .subscribe((accountResults: AccountDataItems[]) => {
        this.sanitizedAccountResults = this.getResult.sanitized(accountResults);
        this.showSpinner = false;
      });
  }
}
