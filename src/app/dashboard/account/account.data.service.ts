import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { map, delay } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AccountDataItems } from './account.data';

@Injectable({
  providedIn: 'root'
})
export class AccountDataService {

  constructor(
    private http: HttpClient
  ) { }

  loadResults(): Observable<AccountDataItems[]> {
    let result;
    result = this.http.get<AccountDataItems[]>('../../assets/results.json').pipe(delay(2000));
    return result.pipe(map(({ content }) => content));
  }

}
