export interface AccountDataItems {
  content: AccountDataItem[];
}

export interface AccountDataItem {
  id: string;
  name: string;
  balance: string;
  currency: string;
  status: string;
  identifiers: Identifier[];
  customerId: string;
  externalReference: string;
}

export interface Identifier {
  type: string;
  accountNumber: string;
  sortCode: string;
}

export interface SanitizedAccountResult {
  accountID: string;
  accountName: string;
  accountNumber: number;
  sortCode: number;
  balance: number;
}
