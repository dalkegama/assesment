/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AccountResultsService } from './account.results.service';

describe('Service: Account.results', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountResultsService]
    });
  });

  it('should ...', inject([AccountResultsService], (service: AccountResultsService) => {
    expect(service).toBeTruthy();
  }));
});
