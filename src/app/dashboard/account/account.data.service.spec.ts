/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';

import { AccountDataService } from './account.data.service';

xdescribe('Service: Account.data', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountDataService]
    });
  });

  it('should ...', inject([AccountDataService], (service: AccountDataService) => {
    expect(service).toBeTruthy();
  }));
});
