import { Injectable } from '@angular/core';
import { SanitizedAccountResult } from './account.data';

@Injectable({
  providedIn: 'root'
})
export class AccountResultsService {

constructor() { }

  newAccountDetailsList(trimmedEvent: any, sanitizedAccountResults: SanitizedAccountResult[]): any[] {
    const searchResultWithMatchingID  = [];

    sanitizedAccountResults.map(data => {
      if (data.accountID === trimmedEvent) {
        searchResultWithMatchingID.push(data);
      }
      if (data.accountName === trimmedEvent) {
        searchResultWithMatchingID.push(data);
      }
    });
    return searchResultWithMatchingID;
  }

  sanitized(accountResults): SanitizedAccountResult[] {
    return accountResults.map((account) => {
      const { id, name, identifiers, balance } = account;
      const [ {accountNumber, sortCode} ] = identifiers;

      return {
        accountID: id,
        accountName: name,
        accountNumber: accountNumber,
        sortCode: this.sortCodeWithDash(sortCode),
        balance: `£${balance}`
      };
    });
  }

  private sortCodeWithDash(sortCode): string {
    return sortCode.replace(/(.{2})/g, '-$1').substr(1);
  }
}
