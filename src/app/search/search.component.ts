import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { SearchSettings } from './search.settings';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  @Input() settings: SearchSettings;
  @Output() searchTerm = new EventEmitter<string>();
  @ViewChild('searchTextInput') private searchInputText: ElementRef;

  searchForm: FormGroup;
  searchValue: string;
  searchSubscription: Subscription;

  ngOnInit(): void {
    this.searchForm = new FormGroup({
    searchfield: new FormControl('',
      [
        Validators.minLength(this.settings.minLength),
        Validators.maxLength(this.settings.maxLength)
      ]
    )
    });

    this.searchSubscription = this.searchForm.controls.searchfield.valueChanges
    .subscribe(term => {
      if ( term === '') { return this.searchTerm.emit(''); }
      if (this.searchForm.valid) { return this.searchValue = term; }
    });
  }

  ngOnDestroy(): void  {
    this.searchSubscription.unsubscribe();
  }

  triggerSearch(): void  {
    if (this.searchForm.valid && this.searchValue !== undefined && this.searchValue !== null && this.searchValue !== '' ) {
      this.searchTerm.emit(this.searchValue);
    }
  }

}
