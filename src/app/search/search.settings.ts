export interface SearchSettings {
  placeHolder: string;
  searchFieldLabel: string;
  minLength?: number;
  maxLength?: number;
}
