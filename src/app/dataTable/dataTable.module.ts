import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataTableComponent } from './dataTable.component';
import { SpaceBeforeUpperCaseMidStringPipe } from '../shared/space-before-uppercase-mid-string.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DataTableComponent,
    SpaceBeforeUpperCaseMidStringPipe
  ],
  exports: [
    DataTableComponent
  ]
})
export class DataTableModule { }
