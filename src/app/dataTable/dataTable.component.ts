import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-data-table',
  templateUrl: './dataTable.component.html',
  styleUrls: ['./dataTable.component.scss']
})
export class DataTableComponent implements OnInit {
  @Input() columns: string[];
  @Input() dataSource: any[];

  columnNames: string[];
  data: any[];

  ngOnInit(): void  {
    this.columnNames = this.columns;
    this.data = this.dataSource;
  }

}
