import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-spinner',
  template: `<div id="loading-spinner"></div>`,
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent { }
