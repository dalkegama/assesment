import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'spaceBeforeUpperCaseMidString'
})
export class SpaceBeforeUpperCaseMidStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let newValue;

    newValue = value.charAt(0).toUpperCase() + value.substr(1);
    return newValue.replace(/([a-z])([A-Z])/g, '$1 $2');
  }

}
